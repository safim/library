<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css">
</head>
<body>

    <div class="container mt-5">
        <div class="row">
            <div class="col-3">
                <div class="list-group">
                    <a href="/authors" class="list-group-item list-group-item-action">Authors</a>
                    <a href="/publications" class="list-group-item list-group-item-action">Publications</a>
                    <a href="/books" class="list-group-item list-group-item-action">Books</a>
                </div>
            </div>
            <div class="col-9">
                <div class="card text-center">
                    <div class="card-header">
                        <h3>PONDIT LIBRARY MANAGEMENT SYSTEM</h3>
                    </div>
                    <div class="card-body">
                        <img src="/img/reading.gif" alt="" draggable="false">
                    </div>
                    <div class="card-footer">Copyright &copy; pondit.com, {{date('Y')}}</div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="js/app.js"></script>
</body>
</html>